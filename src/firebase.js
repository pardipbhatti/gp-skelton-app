import * as firebase from 'firebase';

// Initialize Firebase
  const config = {
    apiKey: "AIzaSyCxBicB6qVHG-XyEXGKBUhJNuOyZHHggCM",
    authDomain: "auth-firebase-30f9b.firebaseapp.com",
    databaseURL: "https://auth-firebase-30f9b.firebaseio.com",
    storageBucket: "auth-firebase-30f9b.appspot.com",
    messagingSenderId: "740386221551"
  };

  export const firebaseApp = firebase.initializeApp(config);
