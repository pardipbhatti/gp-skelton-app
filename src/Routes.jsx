import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import Home from './components/Home';
import SignUp from './components/SignUp';
import SignIn from './components/SignIn';
import Details from './components/Details';


class App extends Component {

  render() {
    return(
      <Router>
        <div>
          <nav className="navbar navbar-inverse">
            <div className="container-fluid">
              <div className="navbar-header">
                <a className="navbar-brand" href="#">WebSiteName</a>
              </div>
              <ul className="nav navbar-nav">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/sign-in">Sing In</Link></li>
                <li><Link to="/sign-up">Sing Up</Link></li>
                <li><Link to="/details/0107">Details</Link></li>
              </ul>
            </div>
          </nav>
          <hr/>

          <Route exact path="/" component={Home}/>
          <Route path="/sign-in" component={SignIn}/>
          <Route path="/sign-up" component={SignUp}/>
          <Route path="/details/:detailsid" component={Details}/>
        </div>
      </Router>
    )
  }
}

export default App;
