import React, { Component } from 'react';
import { firebaseApp } from '../firebase';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      error: {
        message: ''
      }
    }
  }

  signUp() {
    console.log(this.state);
    const { email, password } = this.state;
    firebaseApp.auth().createUserWithEmailAndPassword(email, password)
    .catch(error => {
      console.log('error', error);
      this.setState({error})
    });
  }

  render() {
    return (
      <div className="col-sm-4 col-sm-offset-4">
        <form className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-sm-2">Email:</label>
            <div className="col-sm-10">
              <input type="email" className="form-control" onChange={event => this.setState({email: event.target.value})} id="email" placeholder="Enter email" />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2">Password:</label>
            <div className="col-sm-10">
              <input type="password" className="form-control" onChange={event => this.setState({password: event.target.value})} id="pwd" placeholder="Enter password" />
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <div className="checkbox">
                <label><input type="checkbox" /> Remember me</label>
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="button" onClick={() => this.signUp()} className="btn btn-default">Sign Up</button>
            </div>
          </div>
          <div>{this.state.error.message}</div>
        </form>
      </div>
    );
  }
}

export default SignUp;
